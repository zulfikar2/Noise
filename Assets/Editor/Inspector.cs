﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TextureCreator))]
public class Inspector : Editor {

    private TextureCreator creator;

    public void OnEnable() {
        creator = target as TextureCreator;
        Undo.undoRedoPerformed += refreshCreator;

    }

    public void OnDisable() {
        Undo.undoRedoPerformed -= refreshCreator;
    }

    public override void OnInspectorGUI() {
        EditorGUI.BeginChangeCheck();
        DrawDefaultInspector();
        if (EditorGUI.EndChangeCheck() && Application.isPlaying) {
            (target as TextureCreator).fillTexture();
        }
    }

    private void refreshCreator() {
        if (Application.isPlaying) {
            creator.fillTexture();
        }
    }
}
