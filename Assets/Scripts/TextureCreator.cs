﻿using UnityEngine;
using System.Collections;

public class TextureCreator : MonoBehaviour {
    [Range(2,512)]
    public int resolution = 256;

    private Texture2D texture;
	// Use this for initialization
	void Start () {
	
	}

    void Awake() { 
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    private void OnEnable() {
        if (texture == null)
        {
            texture = new Texture2D(resolution, resolution, TextureFormat.RGB24, true);
            texture.name = "Procedural Texture";
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.filterMode = FilterMode.Trilinear;
            texture.anisoLevel = 9;
            GetComponent<MeshRenderer>().material.mainTexture = texture;
        }
        fillTexture();
    }

    public void fillTexture(){
        if (texture.width != resolution) {
            texture.Resize(resolution, resolution);
        }
        float stepSize = 1f / resolution;
        for (int x = 0; x < resolution; x++) {
            for (int y = 0; y < resolution; y++) {
                texture.SetPixel(x, y, new Color((x + 0.5f) * stepSize % 0.1f, (y + 0.5f) * stepSize % 0.1f, 0f) * 10f);
            }
        }
        texture.Apply();
    }
}

